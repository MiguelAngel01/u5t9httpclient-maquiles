package dam.androidMiguelAngel.u3t9httpclient;

public class GeonamesPlace {

    private String latitude;
    private String length;

    public GeonamesPlace(String latitude, String length) {
        this.latitude = latitude;
        this.length = length;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLength() {
        return length;
    }

}
