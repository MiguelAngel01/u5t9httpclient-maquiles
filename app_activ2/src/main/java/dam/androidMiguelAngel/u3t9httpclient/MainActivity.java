package dam.androidMiguelAngel.u3t9httpclient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private final static String URL_GEONAMES = "http://api.geonames.org/wikipediaSearchJSON";
    private final static String URL_WEATHER = "https://openweathermap.org";
    private final static String USER_NAME = "miguelangel";
    private final static String WEATHER_KEY = "725f8bf8a24768a4b27188e846226c92";
    private final static int ROWS = 10;

    private EditText etPlaceName;
    private Button btSearch;
    private ListView lvSearchResult;
    private ArrayList<String> listSearchResult;
    private ExecutorService executor;
    private ArrayList<GeonamesPlace> geonamesPlaces = new ArrayList<>();
    private String weather = "";
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

    }

    private void setUI() {

        progressBar = findViewById(R.id.progressBar);

        etPlaceName = findViewById(R.id.etPlaceName);

        btSearch = findViewById(R.id.btSearch);
        btSearch.setOnClickListener(this);

        listSearchResult = new ArrayList<>();

        lvSearchResult = findViewById(R.id.lvSearchResult);
        lvSearchResult.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listSearchResult));

        lvSearchResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                progressBar.setVisibility(View.VISIBLE);
                URL url;
                try {
                    String lat = geonamesPlaces.get(position).getLatitude();
                    String lon = geonamesPlaces.get(position).getLength();

                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme("https")
                            .authority("api.openweathermap.org")
                            .appendPath("data")
                            .appendPath("2.5")
                            .appendPath("weather")
                            .appendQueryParameter("lat", lat)
                            .appendQueryParameter("lon", lon)
                            .appendQueryParameter("appid", WEATHER_KEY);

                    url = new URL(builder.build().toString());

                    startBackgroundTask(url);

                } catch (MalformedURLException e){
                    Log.i("URL", e.getMessage());
                }

            }
        });

    }

    @Override
    public void onClick(View v) {

        if (isNetworkAvailable()) {
            progressBar.setVisibility(View.VISIBLE);
            String place = etPlaceName.getText().toString();

            if (!place.isEmpty()) {
                URL url;
                try {
                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme("http")
                            .authority("api.geonames.org")
                            .appendPath("wikipediaSearchJSON")
                            .appendQueryParameter("q", place)
                            .appendQueryParameter("maxRows", String.valueOf(ROWS))
                            .appendQueryParameter("username", USER_NAME);

                    url = new URL(builder.build().toString());

                    startBackgroundTask(url);

                } catch (MalformedURLException e) {
                    Log.i("URL", e.getMessage());
                }
            } else {
                Toast.makeText(this, "Write a place to search", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Sorry, network is not available", Toast.LENGTH_LONG).show();
        }

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etPlaceName.getWindowToken(), 0);

    }

    private void startBackgroundTask(URL url){

        final int CONNECTION_TIMEOUT = 10000;
        final int READ_TIMEOUT = 7000;

        executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        executor.execute(new Runnable() {
            @Override
            public void run() {

                HttpURLConnection urlConnection = null;
                final ArrayList<String> searchResult = new ArrayList<>();

                try {

                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);
                    urlConnection.connect();

                    if (url.toString().contains(URL_GEONAMES)){
                        getData(urlConnection, searchResult);
                    } else {
                        getWeather(urlConnection);
                    }

                } catch (IOException e){
                    Log.i("IOException", e.getMessage());
                } catch (JSONException e){
                    Log.i("JSONException", e.getMessage());
                } finally {
                    if (urlConnection != null) urlConnection.disconnect();
                }

                if (url.toString().contains(URL_GEONAMES)) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            if (searchResult.size() > 0) {
                                ArrayAdapter<String> adapter = (ArrayAdapter<String>) lvSearchResult.getAdapter();
                                adapter.clear();
                                adapter.addAll(searchResult);
                                adapter.notifyDataSetChanged();
                            } else {
                                Toast.makeText(getApplicationContext(), "Not possible to contact " + URL_GEONAMES, Toast.LENGTH_LONG).show();
                            }

                        }
                    });
                }

            }
        });

        try {
            executor.awaitTermination(400, TimeUnit.MILLISECONDS);
            if (!url.toString().contains(URL_GEONAMES)) {
                Toast.makeText(getApplicationContext(), weather.toString(), Toast.LENGTH_LONG).show();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void getWeather(HttpURLConnection urlConnection) throws IOException, JSONException {

        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {

            String resultStream = readStream(urlConnection.getInputStream());

            JSONObject json = new JSONObject(resultStream);

            JSONObject jsonCoord = json.getJSONObject("coord");
            JSONArray jsonWeather = json.getJSONArray("weather");
            JSONObject jsonMain = json.getJSONObject("main");

            String coordinates = "Weather conditions for (" + jsonCoord.getString("lat") + ". " + jsonCoord.getString("lon") + ")\n";
            float celsius = Float.parseFloat(jsonMain.getString("temp")) - 273.15f;
            String temperature = "TEMP: " + celsius + " C\n";
            String humidity = "HUMIDITY: " + jsonMain.getString("humidity") + "%\n";
            String weather = jsonWeather.getJSONObject(0).getString("description");
            this.weather = coordinates + temperature + humidity + weather;

        } else {
            Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
        }

        progressBar.setVisibility(View.INVISIBLE);

    }

    private void getData(HttpURLConnection urlConnection, ArrayList<String> searchResult) throws IOException, JSONException{

        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            String resultStream = readStream(urlConnection.getInputStream());

            JSONObject json = new JSONObject(resultStream);
            JSONArray jArray = json.getJSONArray("geonames");

            if (jArray.length() > 0) {
                geonamesPlaces.removeAll(geonamesPlaces);
                    for (int i = 0; i < jArray.length(); i++){

                        JSONObject item = jArray.getJSONObject(i);
                        searchResult.add(item.getString("summary") + "\nLAT: " + item.getString("lat") + " LON: " + item.getString("lng"));
                        geonamesPlaces.add(new GeonamesPlace(item.getString("lat"), item.getString("lng")));

                    }

            } else {
                searchResult.add("No information found at geonames");
            }
        } else {
            Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
        }

        progressBar.setVisibility(View.INVISIBLE);

    }

    private String readStream(InputStream in) throws  IOException {

        StringBuilder sb = new StringBuilder();

        BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        String nextLine = "";

        while ((nextLine = reader.readLine()) != null){

            sb.append(nextLine);

        }

        return sb.toString();

    }

    @SuppressWarnings("deprecation")
    private boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            Network nw = connectivityManager.getActiveNetwork();
            if (nw == null) return false;

            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);

            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR));
        } else {
            NetworkInfo nwInfo = connectivityManager.getActiveNetworkInfo();
            return nwInfo != null && nwInfo.isConnected();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (executor != null){
            executor.shutdown();
            Log.i("EXECUTOR", "ALL TASKS CANCELLED !!!!!!");
        }
    }
}